import React, { useEffect, useState } from 'react';
import { Table, Input, Button, Space } from 'antd';
import type { TableProps } from 'antd';

interface MatchHistory {
  team: string;
  result: 'win' | 'draw' | 'lost';
}

interface TeamInfo {
  team: string;
  played: number;
  win: number;
  draw: number;
  lost: number;
  points: number;
  matchHistory: MatchHistory[];
  place: number;
}

const TeamTable = () => {
  const [newTeam, setNewTeam] = useState<string>('');

  const [data, setData] = useState<TeamInfo[]>([]);

  const columns: TableProps<TeamInfo>['columns'] = [
    {
      title: 'Place',
      dataIndex: 'place',
      key: 'place',
    },
    {
      title: 'Team',
      dataIndex: 'team',
      key: 'team',
    },
    {
      title: 'Played',
      dataIndex: 'played',
      key: 'played',
    },
    {
      title: 'Win',
      dataIndex: 'win',
      key: 'win',
    },
    {
      title: 'Draw',
      dataIndex: 'draw',
      key: 'draw',
    },
    {
      title: 'Lost',
      dataIndex: 'lost',
      key: 'lost',
    },
    {
      title: 'Points',
      dataIndex: 'points',
      key: 'points',
    },
  ];

  useEffect(() => {
    const data = localStorage.getItem('kickertech_data');
    if (data) {
      const formattedData = JSON.parse(data);
      setData(formattedData);
    }
  }, []);

  const addNewTeam = () => {
    if (newTeam !== '') {
      const teamAlreadyExists: TeamInfo | undefined = data.find(
        (dataObj: TeamInfo) => dataObj.team === newTeam
      );

      if (!teamAlreadyExists) {
        const newData: TeamInfo[] = [...data];

        const newTeamObj: TeamInfo = {
          team: newTeam,
          played: 0,
          win: 0,
          draw: 0,
          lost: 0,
          points: 0,
          matchHistory: [],
          place: newData.length + 1,
        };

        newData.push(newTeamObj);

        const teamsCurrentlyPlaying: string[] = [];

        newData.forEach((originalTeam: TeamInfo) => {
          const { team, matchHistory } = originalTeam;

          newData.forEach((teamToPlay: TeamInfo) => {
            const secondTeam = teamToPlay.team;

            const teamAlreadyPlaying = teamsCurrentlyPlaying.find(
              (teamPlaying: string) => teamPlaying === team
            );

            const secondTeamAlreadyPlaying = teamsCurrentlyPlaying.find(
              (teamPlaying: string) => teamPlaying === secondTeam
            );

            if (team !== secondTeam && !teamAlreadyPlaying && !secondTeamAlreadyPlaying) {
              const teamsAlreadyPlayed = matchHistory.find(
                (history: MatchHistory) => history.team === secondTeam
              );

              if (!teamsAlreadyPlayed) {
                teamsCurrentlyPlaying.push(team);
                teamsCurrentlyPlaying.push(secondTeam);

                // 1 - Win, 2 - Draw, 3 - Lose
                const originalTeamResult = Math.floor(Math.random() * (3 - 1 + 1) + 1);

                if (originalTeamResult === 1) {
                  matchHistory.push({ team: secondTeam, result: 'win' });
                  teamToPlay.matchHistory.push({ team: team, result: 'lost' });
                  originalTeam.win += 1;
                  originalTeam.points += 3;
                  teamToPlay.lost += 1;
                } else if (originalTeamResult === 2) {
                  originalTeam.draw += 1;
                  originalTeam.points += 1;
                  teamToPlay.draw += 1;
                  teamToPlay.points += 1;
                  matchHistory.push({ team: secondTeam, result: 'draw' });
                  teamToPlay.matchHistory.push({ team: team, result: 'draw' });
                } else {
                  originalTeam.lost += 1;

                  teamToPlay.win += 1;
                  teamToPlay.points += 3;
                  matchHistory.push({ team: secondTeam, result: 'lost' });
                  teamToPlay.matchHistory.push({ team: team, result: 'win' });
                }

                teamToPlay.played += 1;
                originalTeam.played += 1;
              }
            }
          });
        });

        newData.sort(function sortDataByPoints(a, b) {
          return b.points - a.points;
        });

        newData.forEach((team: TeamInfo, index: number) => {
          team.place = index + 1;
        });

        setData(newData);
        setNewTeam('');

        localStorage.setItem('kickertech_data', JSON.stringify(newData));
      }
    }
  };

  return (
    <>
      <Space>
        {' '}
        <Input
          placeholder='New team'
          size='large'
          onChange={(e: React.ChangeEvent<HTMLInputElement>) =>
            setNewTeam(e.target.value)
          }
          value={newTeam}
        />{' '}
        <Button size='large' type='primary' onClick={addNewTeam}>
          Add
        </Button>
        <Button
          size='large'
          type='primary'
          onClick={() => localStorage.removeItem('kickertech_data')}
        >
          Clear local storage
        </Button>
      </Space>

      <Table
        columns={columns}
        dataSource={data}
        pagination={false}
        style={{ width: 800, marginTop: 15 }}
      />
    </>
  );
};

export default TeamTable;
