import React from 'react';

import TeamTable from './components/TeamTable';

function App() {
  return (
    <div className='App'>
      <TeamTable />
    </div>
  );
}

export default App;
